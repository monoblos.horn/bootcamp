<?php

require_once 'Animal.php';

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 2
echo $sheep->cold_blooded; // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


// index.php

require_once 'Ape.php';
echo " <br>";
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"



require_once 'Frog.php';
echo "<br>";
$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"


?>